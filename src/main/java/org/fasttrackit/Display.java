package org.fasttrackit;

import org.fasttrackit.activities.Activity;

import static org.fasttrackit.Main.APP_LABEL;
import static org.fasttrackit.Main.APP_NAME;

public class Display {
    public void printAppName() {
        System.out.println(APP_LABEL);
    }

    public void askUserForName() {
        System.out.println("What is your name!");
    }

    public void greetKnownUser(Person user) {
        System.out.println("Welcome to the " + APP_NAME + " " + user.getFullName() + " !");
    }

    public void showActivityMenu() {
        //   System.out.println("Congrats! You have reached " + NO_OF_STEPS + " steps");
        System.out.println("What would you like to do today?");
        System.out.println("1. Swimming");
        System.out.println("2. Running");
        System.out.println("3. Dancing");
        System.out.println("4. Cycling");
        System.out.println("5. Fitness");
        System.out.println("6. Yoga");
    }

    public void showAdditionalActivityMenu() {
        System.out.println("Would to like to add additional activities?");
        System.out.println("Yes");
        System.out.println("No");
    }

    public void printChosenActivity(Activity activity) {
        System.out.println("You have chosen " + activity.getName() + " from the fitness menu");
    }


    public void printMessage(String msg) {
        System.out.println(msg);
    }

    public void goodByeMessage() {
        System.out.println("Bye, see you tomorrow.");
    }

    public void showCyclingActivityMenu() {
        System.out.println("What type of cycling do you like to do today?");
        System.out.println("a. Outdoor cycling");
        System.out.println("b. Indoor cycling");
        System.out.println("c. Spinning cycling");
        System.out.println("d. Soul cycling");
    }




}
