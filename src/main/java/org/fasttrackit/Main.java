package org.fasttrackit;

import org.fasttrackit.activities.*;

import java.util.Scanner;

public class Main {
    public static final String APP_NAME = "Smart Fitness EU";
    public static final String APP_LABEL = " - =" + APP_NAME + "= - ";
    private static final Display display = new Display();
    private static Scanner in;

    public static void main(String[] args) {
        Person user = greetingUser();
        in = authenticateUser(user);
        if (in == null)
            return;
        display.showActivityMenu();
        String userAnswer = interrogateUser(in);
        chooseActivity(userAnswer);
        display.showAdditionalActivityMenu();
        userAnswer = interrogateUser(in);
        while (userAnswer.equalsIgnoreCase("yes")) {
            display.showActivityMenu();
            userAnswer = interrogateUser(in);
            chooseActivity(userAnswer);
            display.showAdditionalActivityMenu();
            userAnswer = interrogateUser(in);
            if (userAnswer.equalsIgnoreCase("no")) {
                break;
            }
        }
        display.goodByeMessage();
    }

    private static String interrogateUser(Scanner scanner) {
        return scanner.nextLine();
    }

    private static void chooseActivity(String activity) {
        switch (activity) {
            case "1": {
                SwimmingActivity swimming = new SwimmingActivity("light");
                display.printChosenActivity(swimming);
                display.printMessage(swimming.askAboutNumberOfPools());
                String userInput = interrogateUser(in);
                int noOfPools = Integer.parseInt(userInput);
                swimming.withNoOfPools(noOfPools);
                display.printMessage(swimming.provideSwimStatistics());
                break;
            }
            case "2": {
                RunningActivity running = new RunningActivity("Light");
                display.printChosenActivity(running);
                display.printMessage(running.askAboutNumberOfKm());
                String userInput = interrogateUser(in);
                int noOfKm = Integer.parseInt(userInput);
                running.withNoOfKm(noOfKm);
                display.printMessage(running.provideStatistics());
                break;
            }
            case "3": {
                DancingActivity dancing = new DancingActivity("Light");
                display.printChosenActivity(dancing);
                display.printMessage(dancing.askAboutNumberOfHours());
                String userInput = interrogateUser(in);
                int noOfHours = Integer.parseInt(userInput);
                dancing.withNoOfHours(noOfHours);
                display.printMessage(dancing.provideStatistics());
                break;
            }
            case "4": {
                CyclingActivity cycling = new CyclingActivity("Light");
                display.printChosenActivity(cycling);
                display.showCyclingActivityMenu();
                String userAnswerForCycling = interrogateUser(in);
                if (userAnswerForCycling.equals("a")) {
                    System.out.println("You have chosed Outdoor cycling from Cycling menu.");
                }
                if (userAnswerForCycling.equals("b")) {
                    System.out.println("You have chosed Indoor cycling from Cycling menu.");
                }
                if (userAnswerForCycling.equals("c")) {
                    System.out.println("You have chosed Spinning cycling from Cycling menu.");
                }
                if (userAnswerForCycling.equals("d")) {
                    System.out.println("You have chosed Soul cycling from Cycling menu.");
                }
                display.printMessage(cycling.askAboutNoOfKm());
                String userInput = interrogateUser(in);
                int noOfKm = Integer.parseInt(userInput);
                cycling.withNoOfKm(noOfKm);
                display.printMessage(cycling.provideCyclingStatistics());
                break;
            }
            case "5": {
                FitnessActivity fitness = new FitnessActivity("Light");
                display.printChosenActivity(fitness);
                display.printMessage(fitness.askUserAboutNoOfSessions());
                String userInput = interrogateUser(in);
                int noOfSessions = Integer.parseInt(userInput);
                fitness.withNoOfSessions(noOfSessions);
                display.printMessage(fitness.provideStatistics());
                break;
            }
            case "6": {
                YogaActivity yoga = new YogaActivity("Light");
                display.printChosenActivity(yoga);
                display.printMessage(yoga.askAboutNumberOfMeetings());
                String userInput = interrogateUser(in);
                int noOfMeetings = Integer.parseInt(userInput);
                yoga.withNoOfMeetings(noOfMeetings);
                display.printMessage(yoga.provideStatistics());
                break;
            }
        }
    }

    private static Scanner authenticateUser(Person user) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please Enter Your PIN number: ");
        String pinCode = interrogateUser(scanner);
        if (!user.getPinCode().equals(pinCode)) {
            System.out.println("Wrong pin!");
            return null;
        }
        System.out.println("Successfully logged in.");
        return scanner;
    }

    private static Person greetingUser() {
        display.printAppName();
        Scanner scanner = new Scanner(System.in);
        display.askUserForName();
        String name = interrogateUser(scanner);
        Person user = new Person(name, "John", "other", "1860000001", "9999", "5444");
        display.greetKnownUser(user);
        return user;
    }


}