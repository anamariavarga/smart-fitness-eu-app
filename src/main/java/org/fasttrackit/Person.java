package org.fasttrackit;

public class Person {
    private final String name;
    private final String firstName;
    private final String gender;

    private final String id;
    private final String pinCode;
    private final String completedSteps;

    public Person(String name, String firstName, String gender, String id, String pinCode, String completedSteps1) {
        this.name = name;
        this.firstName = firstName;
        this.gender = gender;
        this.id = id;
        this.pinCode = pinCode;
        this.completedSteps = completedSteps1;
    }

    public String getPinCode() {
        return pinCode;
    }

    public String getFullName() {

        return name + " " + firstName;
    }

      public String getCompletedSteps () {
        return completedSteps;
    }
}

