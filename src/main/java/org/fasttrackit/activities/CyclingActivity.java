package org.fasttrackit.activities;

public class CyclingActivity extends Activity {
    private final static String CYCLING = "Cycling";
    private static final int BURNT_CALORIES_PER_KM = 300;
    private static final int MINUTES_OF_QUALITY_TIME_PER_KM = 3;
    int noOfKm = 0;

    public CyclingActivity(String intensity) {
        super(intensity, CYCLING);
    }

    public String askAboutNoOfKm() {
        return "How many km did you cycling today?";
    }

    public void withNoOfKm(int noOfKm) {
        this.noOfKm = noOfKm;
    }

    public String provideCyclingStatistics() {
        return "During your " + noOfKm + " km route of cycling, you burnt " + calculateBurntCyclingCalories() + " calories. But the most beautiful thing is that you gain " + calculateMinutesOfQualityTime() + " minutes of quality time! Congratulations!";
    }

    public int calculateBurntCyclingCalories() {
        int burntCalories = this.BURNT_CALORIES_PER_KM * noOfKm;
        return burntCalories;
    }

    public int calculateMinutesOfQualityTime() {
        int minutesOfQualityTime = this.noOfKm * MINUTES_OF_QUALITY_TIME_PER_KM;
        return minutesOfQualityTime;
    }
}
