package org.fasttrackit.activities;

import org.fasttrackit.activities.Activity;

public class DancingActivity extends Activity {
    private final static String DANCING = "Dancing";
    private final static int BURNT_CALORIES_PER_HOUR = 300;
    private final static int HOURS_TO_EARN_VIP_MEMBERSHIP = 50;
    private int noOfHours = 0;


    public DancingActivity(String intensity) {
        super(intensity, DANCING);
    }

    public String askAboutNumberOfHours() {
        return "How many hours did you practice dancing today?";
    }

    public void withNoOfHours(int noOfHours) {
        this.noOfHours = noOfHours;
    }

    public String provideStatistics() {
        return "You have dance " + noOfHours + " hours and you burnt " + calculateBurntDancingCalories() + " calories and you have to complete " + hoursToBecomeVipMembership() + " more hours of dancing in order to became a Vip Membership!";
    }

    public int hoursToBecomeVipMembership() {
        int hoursToVip = this.HOURS_TO_EARN_VIP_MEMBERSHIP - noOfHours;
        return hoursToVip;
    }

    public int calculateBurntDancingCalories(){
        int dancingCalories = this.noOfHours * BURNT_CALORIES_PER_HOUR;
        return dancingCalories;
    }
}

