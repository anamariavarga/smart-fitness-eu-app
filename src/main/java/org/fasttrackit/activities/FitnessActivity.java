package org.fasttrackit.activities;

public class FitnessActivity extends Activity {
    private final static String FITNESS = "Fitness";
    private final static int BURNT_CALORIES_PER_HOUR = 200;
    private final static int HOURS_PER_SESSION = 3;
    private int noOfSessions = 0;

    public FitnessActivity(String intensity) {
        super(intensity, FITNESS);
    }

    public String askUserAboutNoOfSessions() {
        return "How many fitness sessions did you complete?";
    }

    public void withNoOfSessions(int noOfSessions) {
        this.noOfSessions = noOfSessions;
    }

    public String provideStatistics() {
        return "You have reached " + calculateNoOfHours() + " hours of fitness class and you have burnt " + calculateBurntCalories() + " calories.";
    }
    public int calculateNoOfHours (){
        int noOfHours = noOfSessions * HOURS_PER_SESSION;
        return noOfHours;
    }
    public int calculateBurntCalories (){
        int burntCalories = calculateNoOfHours() * BURNT_CALORIES_PER_HOUR;
        return burntCalories;
    }
}
