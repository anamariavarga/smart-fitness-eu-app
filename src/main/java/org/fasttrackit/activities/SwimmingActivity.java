package org.fasttrackit.activities;

public class SwimmingActivity extends Activity {
    private static final String SWIMMING = "Swimming";
    private static final int POOL_LENGTH = 50;
    private static final int BURNT_CALORIES_PER_POOL = 200;
    private int noOFPools = 0;

    public SwimmingActivity(String intensity) {

        super(intensity, SWIMMING);
    }

    public String askAboutNumberOfPools() {
        return "How many swimming pools did you complete today ?";
    }

    public String provideSwimStatistics() {
        return "You have burnt " + getBurntCalories() + " calories in " + getSwimDistance () + " meters swim. ";
    }

    public void withNoOfPools(int noOFPools) {
        this.noOFPools = noOFPools;
    }

    public int getBurntCalories() {
        int calories = BURNT_CALORIES_PER_POOL * this.noOFPools;
        return calories;
    }

    public int getSwimDistance() {
        int distance = POOL_LENGTH * this.noOFPools;
        return distance;
    }
}
