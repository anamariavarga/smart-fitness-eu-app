package org.fasttrackit.activities;

public class YogaActivity extends Activity {
    private final static String YOGA = "Yoga";
    private final static int HOURS_TO_COMPLETE_LEVEL_1 = 100;
    private final static int HOURS_PER_MEETING = 2;
    private int noOfMeetings = 0;

    public YogaActivity(String intensity) {
        super(intensity, YOGA);
    }

    public String askAboutNumberOfMeetings() {
        return "How many meetings did you reached until today?";
    }

    public void withNoOfMeetings(int noOfMeetings) {
        this.noOfMeetings = noOfMeetings;
    }

    public String provideStatistics() {
        return "You have reached a number of " + calculateNoOfHours() + " hours of practice so you have " + calculateHoursToLevel1() + " hours left to complete Level 1.";
    }

    public int calculateNoOfHours() {
        int hours = HOURS_PER_MEETING * this.noOfMeetings;
        return hours;
    }

    public int calculateHoursToLevel1() {
        int hoursToLevel1 = HOURS_TO_COMPLETE_LEVEL_1 - HOURS_PER_MEETING * this.noOfMeetings;
        return hoursToLevel1;
    }
}
